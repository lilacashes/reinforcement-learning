# Reinforcement Learning sample programs

Page numbers from the e-book edition viewed with `calibre`

## Done
* Algorithm 2-1: epsilon-greedy bandit algorithm (p.78): 
  [website_a_b.py](reinforcement_learning/website_a_b.py)

## To Do
* Improving the epsilon-greedy algorithm (p.85)
* Inventory control (p.92)
* Simulation using the state-value function (p.109)
* Algorithm 2-3: On-Policy MC algorithm (p.123)
* Algorithm 2-4: Value iteration for generating an optimal policy (p.130)
* Algorithm 3-1: Q-learning (off-policy TD) (p.144)
* Algorithm 3-2: SARSA (on-policy TD) (p.148)
* Algorithm 3-3: n-step SARSA (p.176)
* Algorithm 3-3: SARSA(lambda) (p.184)
* 
