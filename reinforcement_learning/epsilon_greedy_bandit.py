__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from collections import defaultdict
from dataclasses import dataclass
from random import random, choice
from typing import Dict, List
from sys import maxsize

MIN_REWARD = -maxsize
min_reward_factory = lambda: MIN_REWARD


class Action:
    pass


class State:
    pass


@dataclass
class Environment:
    state: State = None

    def run_action(self, action: Action) -> float:
        pass


def epsilon_greedy(
        epsilon: float, actions: List[Action], environment: Environment, num_episodes: int
) -> List[float]:
    """
    :param epsilon: exploration probability
    :param actions: list of possible Actions
    :param environment: Environment
    :param num_episodes: number of episodes to run
    :return:
    """
    r_avg: Dict[Action, float] = defaultdict(float)
    count: Dict[Action, int] = defaultdict(int)
    plot_values: List[float] = []
    for i in range(num_episodes):
        if random() < epsilon:
            action = choice(actions)
        else:
            action = choice(argmax(actions, environment))

        reward = environment.run_action(action)  # present action to environment and get reward
        count[action] += 1
        r_avg[action] += (reward - r_avg[action]) / count[action]
        plot_values.append(count[actions[0]] / (count[actions[0]] + count[actions[1]]))

    return plot_values


def argmax(actions: List[Action], environment: Environment) -> List[Action]:
    rewards: Dict[Action, float] = defaultdict(min_reward_factory)
    max_reward = MIN_REWARD
    for action in actions:
        rewards[action] = environment.run_action(action)
        if rewards[action] > max_reward:
            max_reward = rewards[action]
    return [action for action in actions if rewards[action] == max_reward]

