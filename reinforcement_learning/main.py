__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from typing import List, Dict

import matplotlib.pyplot as plt

from reinforcement_learning.epsilon_greedy_bandit import epsilon_greedy
from reinforcement_learning.website_a_b import WebsiteAB, ButtonAction


def main():
    environment = WebsiteAB()
    plot_values: Dict[float, List[float]] = {}
    for epsilon in [0.1, 0.5, 1.0]:
        plot_values[epsilon] = epsilon_greedy(
            epsilon, [ButtonAction('red'), ButtonAction('green')], environment, 1000
        )
    fig, ax = plt.subplots()  # Create a figure containing a single axes.
    for plot in plot_values.keys():
        ax.plot(plot_values[plot], label=str(plot))
    ax.legend()
    plt.show()


if __name__ == '__main__':
    main()